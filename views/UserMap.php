<div class="row" id="row-map-user">
	<div id="add-user-map" class="btn-group">
	    <button class="btn btn-default" onclick="toggle()">
	    	Map User
	    </button>
	</div>
	<div class="col-md-12">

		<?php echo $this->template->cardOpen('Mapped User');?>
		
		<?php echo $this->template->cardBodyOpen();?>
			<table class="table table-hover table-stripped" id="grid-userMap">
				<thead>
					<tr>
						<th data-column-id="username">Username</th>
						<th data-column-id="fullname" data-formatter="fullname">Fullname</th>
					</tr>
				</thead>
			</table>
		<?php echo $this->template->cardBodyClose();?>
	</div>
</div>

<div class="row" id="row-add-map" style="display: none;">
	<div class="col-md-12">
		<button class="btn btn-primary btn-xs" onclick="toggle()">
			<span class="fa fa-arrow-left"></span> Back
		</button>
		<?php echo $this->template->cardOpen('Map User');?>

		<?php echo $this->template->cardBodyOpen();?>
			<?php
				$this->load->view('userChooser',$data = array('callBackEvent' => 'addToMap','mode' => 'NOTEXISTSIN_USERMAP'));
			?>
		<?php echo $this->template->cardBodyClose();?>
	</div>
</div>

<script type="text/javascript">

	var table = $("#grid-userMap").DataTable({
		processing:true,
		serverSide:true,
		ajax: {
			url:"<?php echo site_url('user/UserMap/loadDatatable') ?>",
			type:"POST",
		},
		columns:[
			{data:"username"},
			{
				data:"fullname",
				render:function(data,type,row){
					return row.first_name + ' ' + row.last_name
				},
			},
		]
	});

	$("#grid-userMap_filter").append($("#add-user-map"));

	initUserChooser();

	function toggle(){
		$("#row-map-user, #row-add-map").slideToggle("slow"); 
	}

	function addToMap(data){
		id_user = data.id_user;

		if (! $.fn.DataTable.isDataTable('#accountChooser') ) {
			$("#accountChooser").DataTable({
				serverSide:true,
				processing:true,
				ajax : {
					url : "<?php echo site_url('rbac/account/loadList') ?>",
					type:"POST"
				},
				columns : [
					{data:"id_account"},
					{data:"username"},
					{data:"role_name"},
					{
						data:"account_status",
						render:function(data,type,row){
							if(row.account_status == 1){
								return '<span class="badge badge-success">Active</span>';
							}else if(row.account_status == 2){
								return '<span class="badge badge-warning">Pending</span>';						
							}else if(row.account_status == 3){
								return '<span class="badge badge-danger">Banned</span>';
							}
						}
					},
					{
						data:"action",
						render:function(data,type,row){
							return '<button class="btn btn-success btn-xs chooseAccount" id_account="'+row.id_account+'">Choose!</button>';
						}
					},

				]
			});
		}

		$("#accountChooser tbody").on('click','.chooseAccount',function(){
			var id_account = $(this).attr('id_account');
			var confirmation = confirm('Are you sure want to map '+id_user+' to '+id_account+' ?');
			if(confirmation){
				$.ajax({
					url:"<?php echo site_url('user/UserMap/save')?>",
					data : {
						id_user : id_user,
						id_account : id_account,
					},
					type:"POST",
					success:function(res){
						res = JSON.parse(res);
						if(res.status){
							custom_notification('success','Success mapping user');
						}else{
							custom_notification('danger','Failed : '+res.info);
						}
						$(".modal").modal("hide");
						toggle();
					}
				});
			}else{
				custom_notification('info','Nothing changed.');
				$(".modal").modal('hide');
			}
			$("#grid-userMap").DataTable().ajax.reload();
		});
		$("#accountChooserModal").modal('show');
	}
</script>

<div id="accountChooserModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Choose Account</h4>
            </div>
            <div class="modal-body">
            	<table class="table table-hover table-stripped" id="accountChooser" style="width: 100%">
            		<thead>
            			<tr>
            				<th data-column-id="id_account">ID Account</th>
            				<th data-column-id="username">Username</th>
            				<th data-column-id="role_name">Role</th>
            				<th data-column-id="account_status" data-formatter="account_status">Account Status</th>
            				<th data-column-id="account_chooser" data-formatter="account_chooser">Choose</th>
            			</tr>
            		</thead>
            	</table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

