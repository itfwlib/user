<style type="text/css">
	.userFile-picture{
		width: 80px; 
		height: 80px; 
		border: 2px solid;
		/*min-width: 100%;*/
	}
	.container {
	  /*position: relative;*/
	  /*width: 50%;*/
	}
	.image {
	  display: block;
	  width: 100%;
	  height: auto;
	}
	.overlay {
		margin-left: 15px;
		position: absolute;
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		height: 80px;
		width: 80px;
		opacity: 0;
		transition: .5s ease;
		background-color: #008CBA;
		cursor: pointer;
	}
	.container:hover .overlay {
	  opacity: 0.5;
	}
	.text {
	  color: white;
	  font-size: 10px;
	  position: absolute;
	  top: 50%;
	  left: 50%;
	  -webkit-transform: translate(-50%, -50%);
	  -ms-transform: translate(-50%, -50%);
	  transform: translate(-50%, -50%);
	  text-align: center;
	}
	.panel {
	    overflow: auto;
	}
	.hidden{
		display: none;
	}
	.pictureDesc{
		overflow: auto;
	}
</style>
<div class="row">
	<div class="col-md-6">
		<?php echo $this->template->cardOpen('User File');?>
		
		<?php echo $this->template->cardBodyOpen();?>
			<form method="POST" action="#" id="userFileForm-file" class="form-horizontal" enctype="multipart/form-data">
				
			</form>
		<?php echo $this->template->cardBodyClose();?>
	</div>	
	<div class="col-md-6">
		<?php echo $this->template->cardOpen('User Picture');?>
		
		<?php echo $this->template->cardBodyOpen();?>
			<div id="userFile-pictureLibrary">
				
			</div>
			<form action="#" id="userFileForm-picture" >
				<div class="row">
					<button class="btn btn-primary btn-save" id="btnSave-Picture">
						<span class="fa fa-check"></span> Save
					</button>
				</div>
			</form><br>
		<?php echo $this->template->cardBodyClose();?>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		initUserFile();
	});
	function initUserFile(){
		$.ajax({
			url:"<?php echo site_url('user/userFile/getSetting')?>",
			success:function(res){
				res = JSON.parse(res);
				if(!res.status){
					custom_notification('danger','Failed to load user file settings');
					return;
				}
				picture_setting = res.data.picture;
				file_setting = res.data.file;
				$.each(file_setting,function(column,key){
					txt = '';
					if(key.show){
						txt += '<div class="form-group">';
						txt += '	<label class="control-label col-md-3">'+key.alias+'</label>';
						txt += '	<div class="col-md-9">';
						txt += '		<input type="file" class="form-control" name="'+key.name+'" id="'+key.name+'">';
						txt += '	</div>';
						txt += '</div>';
					}
					$("#userFileForm-file").append(txt);
				});
				counter = 1;
				$.each(picture_setting,function(column,key){
					txt = '';
					if(key.show){
						if(counter / 6 == 1){
							txt += '<div class="row">';
						}
						txt += 		'<div class="col-md-2 container" onclick="pictureSelector(\''+key.name+'\')">';
						txt += 		'	<img src="<?php echo base_url('assets/user_upload/user_picture/default.jpg')?>" alt="user-picture" class="userFile-picture" id="userFile-picture_'+key.name+'">';
						txt += 		'	<div class="overlay">';
						txt += 		'	<div class="text">Change Picture</div>';
						txt += 		'	</div>';
						txt += 		'	<small class="pictureDesc">'+key.alias+'</small> ';
						txt += 		'</div>';
						if(counter / 6 == 0){
							counter = 0;
							txt += '</div>';
						}
						$("#userFile-pictureLibrary").append(txt);
						txt = '';
						txt += '<input type="file" name="'+key.name+'" id="userFileForm-picture_'+key.name+'" class="hidden">';
						$("#userFileForm-picture").append(txt);
						counter++;
					}
				});
				$(".hidden").on('change',function(){
					if (this.files && this.files[0]) {
						name = $(this).attr('name');
		                var reader = new FileReader();
		                reader.onload = function (e) {
							$("#userFile-picture_"+name).attr('src', e.target.result);
		                };

		                reader.readAsDataURL(this.files[0]);
		            }
				});
			}
		});
	}

	function pictureSelector(id){
		$("#userFileForm-picture_"+id).trigger('click');
	}

</script>
