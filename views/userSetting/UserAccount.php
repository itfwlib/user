<div class="row">
	<div class="col-md-9">
		<form class="form-horizontal" id="rowAccount">
			<input type="hidden" class='form-control' value="" readonly name="id_account" id="id_account">
			<input type="hidden" name="pass_role" value="1">
			<input type="hidden" class='form-control' readonly name="account_status" id="account_status">
			<div class="form-group">
				<label class="control-label col-md-3">Username</label>
				<div class="col-md-9">
					<input type="text" class='form-control' value="" readonly name="username" id="username">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Email</label>
				<div class="col-md-9">
					<input type="email" class='form-control' name="email" id="email">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Password</label>
				<div class="col-md-9">
					<input type="password" class='form-control' value="" name="password" id="password">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Confirmation Password</label>
				<div class="col-md-9">
					<input type="password" class='form-control' value="" name="confPass" id="confPass">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"></label>
				<div class="col-md-9">
					<button class="btn btn-primary" id="btnSaveAccount">
						<span class="fa fa-check"></span> Save
					</button>
				</div>
			</div>
		</form>
			
	</div>
</div>
