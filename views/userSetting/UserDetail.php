<?php
	$sessData = $this->session->userdata();
	$this->load->config('User_detail');
	$userDetailColumn = $this->config->item('user_detail')['column'];
?>

<!-- Row user detail -->
<style type="text/css">
	#userDetail-picture{
		width: 250px; 
		height: 250px; 
		border: 2px solid;
		min-width: 100%;
	}
	.container {
	  /*position: relative;*/
	  /*width: 50%;*/
	}

	.image {
	  display: block;
	  width: 100%;
	  height: auto;
	}

	.overlay {
		margin-left: 15px;
		position: absolute;
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		height: 250px !important;
		width: 250px !important;
		opacity: 0;
		transition: .5s ease;
		background-color: #008CBA;
		cursor: pointer;
	}

	.container:hover .overlay {
	  opacity: 0.5;
	}

	.text {
	  color: white;
	  font-size: 20px;
	  position: absolute;
	  top: 50%;
	  left: 50%;
	  -webkit-transform: translate(-50%, -50%);
	  -ms-transform: translate(-50%, -50%);
	  transform: translate(-50%, -50%);
	  text-align: center;
	}
	.panel {
	    overflow: auto;
	}	
</style>

<div class="row" id="rowUserDetail">
	<div class="col-md-9">
		<form id="userDetailForm" class="form-horizontal" enctype="multipart/form-data">
			<input type="hidden" name="id_user" id="detail-id_user">
			<input type="file" name="picture" id="picture" style="display: none;">
			<div class="form-group">
				<label class="control-label col-md-3">First Name</label>
				<div class="col-md-9">
					<input type="text" class='form-control' name="first_name" id="first_name">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Last Name</label>
				<div class="col-md-9">
					<input type="text" class='form-control' name="last_name" id="last_name">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Gender</label>
				<div class="col-md-9">
					<select name="gender" class="form-control" id="gender">
						<option value="f">Female</option>
						<option value="m">Male</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Birth Date</label>
				<div class="col-md-9">
					<input type="date" class='form-control' name="birth_date" id="birth_date">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Birth Place</label>
				<div class="col-md-9">
					<input type="text" class='form-control' name="birth_place" id="birth_place">
				</div>
			</div>
			<?php
				if(isset($userDetailColumn)){
					foreach ($userDetailColumn as $key => $index) {?>
						<?php if ($index['show']): ?>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo $index['alias']?></label>
								<div class="col-md-9">
									<input type="<?php echo $index['type']?>" class='form-control' value="" name="<?php echo $index['name'] ?>" id="<?php echo $index['name'] ?>">
								</div>
							</div>
						<?php endif ;
					}
				}
			?>
			<div class="form-group">
				<label class="control-label col-md-3"></label>
				<div class="col-md-9">
					<button type="button" class="btn btn-primary" id="btnSaveDetail">
						<span class="fa fa-check"></span> Save
					</button>
					<button type="button" class="btn btn-danger" id="resetDetail">
						Reset
					</button>
				</div>
			</div>
	</div>
	<div class="col-md-2 container">
		<img src="" id="userDetail-picture" alt="profile-picture" class="image">
		<div class="overlay">
		<div class="text">Change Picture</div>
		</div>
		<small>Click image to change</small> 
		</form>
	</div>
</div>

<!-- Row user detail -->

<script type="text/javascript">
	var id_account;
	var rowUserDetail = $("#rowUserDetail");
	$(document).ready(function(){
		// Checking session Data
			<?php
				if(isset($sessData['id_account'])){?>
					id_account = "<?php echo $sessData['id_account']?>";
			<?php
				}
			?>

			if(id_account == null){
				custom_notification('danger','ID Account is invalid');
				$("#rowUserDetail").remove();
			}

		$("#resetDetail").click(function(){
			initDetail();
		});
		$("#btnSaveDetail").click(function(){
			$("#userDetailForm").submit();
		});
		$("#userDetailForm").on('submit',function(e){
			buttonSpinner('btnSaveDetail');
			e.preventDefault();
			var formData = new FormData(this);
			$.ajax({
				url:"<?php echo site_url('user/userDetail/save')?>",
				data : formData,
				type:"POST",
				cache:false,
	            contentType: false,
	            processData: false,
	            success:function(res,a,b){
	            	try{
	            		res = JSON.parse(res);
	            		if(res.status){
	            			custom_notification("success",'Success updating detail.');
	            		}else{
	            			custom_notification('danger','Failed updating user detail');
	            		}
	            	}catch(error){
            			custom_notification('danger','<span class="fa fa-frown-o"></span> Something went wrong. Please Contact administrator');
	            	}
	            	
	            },
	            complete:function(){
	            	console.log('complete');
            		removeButtonSpinner('btnSaveDetail');
	            	initDetail();
	            },
	            error:function(){
	            	console.log('error');
            		removeButtonSpinner('btnSaveDetail');
	            }
			});
		});
		$(".container").click(function(){
			$("#picture").trigger('click');
		});

		$("#picture").on('change',function(){
			if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#userDetail-picture')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(this.files[0]);
            }
		});
	});
	function initDetail(){
		$("#picture").val('');
		$("#rowUserDetail").hide();
		$("#rowUserDetail").parent().append(spinner());
		$.ajax({
			url:"<?php echo site_url('user/userDetail/load')?>",
			data:{
				mode : 'LOADBY_IDACCOUNT',
				id_account : id_account,
			},
			type:"POST",
			success:function(res){
				res = JSON.parse(res);
				data = res.data;
				rowUserDetail.show();
				removeSpinner();

				if(data.picture == null || data.picture == ''){
					picture = 'default.png';
				}else{
					picture = data.picture;
				}

				<?php
					foreach ($userDetailColumn as $key => $value) {?>
						$("#<?php echo $value['name']?>").val(data.<?php echo $value['name']?>);
				<?php
					}
				?>

				$("#birth_date").val(data.birth_date);
				$("#birth_place").val(data.birth_place);
				$("#gender").val(data.gender);
				$("#detail-id_user").val(data.id_user);
				$("#first_name").val(data.first_name);
				$("#last_name").val(data.last_name);
				$("#userDetail-picture").attr('src','<?php echo base_url('assets/user_upload/profile_picture/') ?>'+picture);
			}
		});
	}

</script>