<?php
	$this->load->model('UserGroup_model');
?>

<div class="row" id="row-data-user-group">
	<div class="col-md-12">
		<div id="add-user-group" class="btn-group">
		    <button class="btn btn-default">
		    	Add User Group
		    </button>
		</div>
		<?php echo $this->template->cardOpen('User Management');?>

		<?php echo $this->template->cardBodyOpen();?>
			<table class="table table-condensed table-hover table-striped" id="grid-user-group">
				<thead>
					<tr>
						<?php
							foreach ($this->UserGroup_model->_column as $data) {?>
								<th data-column-id="<?php echo $data['columnName']?>">
									<?php
										if(!empty($data['alias'])){
											echo $data['alias'];
										}else{
											echo ucwords(str_replace('_',' ',$data['columnName']));
										}
									?>
								</th>
						<?php
							}
						?>

						<th data-column-id="action" data-formatter="action">Tools</th>
					</tr>
				</thead>
			</table>
		<?php echo $this->template->cardBodyClose();?>
	</div>
</div>
<div style="display: none;" class="row" id="row-manage-user-group">
	<div class="col-md-3">
		<button class="btn btn-default" onclick="back()">
			Back
		</button>
	</div>
	<br><br>
	<?php
		$this->load->view('manageUserGroup');
	?>
</div>

<script type="text/javascript">

	$(document).ready(function(){
	    $('.bootgrid-header .actionBar .actions').append($('#add-user-group'));

	    $("#add-user-group").click(function(){
	    	$("#addUserGroupModal").modal('show');
	    	$("#user_group_id").val(0);
			$("#prefixModalUserGroup").text('Add');
			$("#form-user-group")[0].reset();
	    });
	    initUserChooser();
	});

	function back(){
		$("#row-manage-user-group").slideUp();
		$("#row-data-user-group").slideDown();
	}

	$("#grid-user-group").bootgrid({
	    ajax: true,
	    url: "<?php echo site_url('user/userGroup/loadList')?>",
	    formatters:{
	    	"action":function(column,row){
	    		txt = '';
	    		// Button Edit
	    		txt += '<button ';
	    		txt += '	title="Edit"';
	    		txt += '	class="btn btn-default btn-xs edit"';

	    		<?php foreach ($this->UserGroup_model->_column as $data): ?>

	    			txt += '<?php echo $data['columnName'] ?>="'+row.<?php echo $data['columnName']?>+'"';

	    		<?php endforeach ?>

	    		txt += '>';
	    		txt += '		<span class="fa fa-pencil"></span>';
	    		txt += '</button> ';

	    		// button delete

	    		txt += '<button ';
	    		txt += '	title="Delete"';
	    		txt += '	class="btn btn-danger btn-xs delete"';

	    		<?php foreach ($this->UserGroup_model->_column as $data): ?>

	    			txt += '<?php echo $data['columnName'] ?>="'+row.<?php echo $data['columnName']?>+'"';

	    		<?php endforeach ?>

	    		txt += '>';
	    		txt += '		<span class="fa fa-trash"></span>';
	    		txt += '</button> ';

	    		// button manage member

	    		txt += '<button ';
	    		txt += '	title="Manage Group"';
	    		txt += '	class="btn btn-default btn-xs manage-group"';

	    		<?php foreach ($this->UserGroup_model->_column as $data): ?>

	    			txt += '<?php echo $data['columnName'] ?>="'+row.<?php echo $data['columnName']?>+'"';

	    		<?php endforeach ?>

	    		txt += '>';
	    		txt += '		<span class="fa fa-user"></span>';
	    		txt += '</button> ';


	    		return txt;
	    	},
	    	"gender":function(columnm,row){
	    		if(row.gender == 'f'){
	    			return 'Female';
	    		}else{
	    			return 'Male';
	    		}
	    	},
	    	"full_name":function(column,row){
	    		return row.first_name + ' ' + row.last_name;
	    	}
	    }
	}).on("loaded.rs.jquery.bootgrid", function (e) {
		$(".edit").click(function(){	
			$("#addUserGroupModal").modal('show');

			<?php foreach ($this->UserGroup_model->_column as $data): ?>
				$("#<?php echo $data['columnName']?>").val(
					$(this).attr('<?php echo $data['columnName']?>')
				);
			<?php endforeach ?>
			$("#prefixModal").text('Edit');
		});

		$(".delete").click(function(){
			$("#deleteUserGroupModal").modal('show');
			$("#deleteUserGroupId").val($(this).attr('user_group_id'));
			$("#deleteUserGroupName").text($(this).attr('user_group_name'));
		});

		$(".manage-group").click(function(){
			$("#row-manage-user-group").slideDown();
			$("#row-data-user-group").slideUp();
			user_group_id = $(this).attr('user_group_id');
			user_group_name = $(this).attr('user_group_name');
			data = {
				"user_group_id" : user_group_id,
				"user_group_name" : user_group_name,
			};
			initManageUserGroup(data);
		});
	});

	function deleteRow(){
		delete_id = $("#deleteUserGroupId").val();
		$.ajax({
			url:"<?php echo site_url('user/userGroup/delete')?>",
			data : { 
				id : delete_id,
			},
			type:"POST",
			success:function(res){
				res = JSON.parse(res);
				if(res.status){
					custom_notification('success','Success deleting data');
				}else{
					custom_notification('danger',res.info);
				}
				$(".modal").modal('hide');
				$("#grid-user-group").bootgrid('reload');
			}
		});
	}
</script>

<!-- Modal Add -->
<div id="addUserGroupModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> <span id="prefixModal">Add</span> User Type</h4>
            </div>
            <div class="modal-body">
                <?php
                	echo $this->load->view('addUserGroup');
                ?>
            </div>
            <div class="modal-footer">
            	<button class="btn btn-primary" onclick="saveUserGroup()">
            		<span class="fa fa-check" id="symbSaveAddUserType"></span> Save 
            	</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Delete -->
<div id="deleteUserGroupModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete User Group
            </div>
            <div class="modal-body">
            	Are you sure want to delete <b id="deleteUserGroupName"></b> ? 
                <input type="hidden" name="deleteUserGroupId" id="deleteUserGroupId">
            </div>
            <div class="modal-footer">
            	<button class="btn btn-primary" onclick="deleteRow()">
            		Delete
            	</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

