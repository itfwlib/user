<?php
	$this->load->model('user_model');
?>

<div id="add-user" class="btn-group">
    <button class="btn btn-default">
    	Add User
    </button>
</div>
<?php echo $this->template->cardOpen('User Management');?>

<?php echo $this->template->cardBodyOpen();?>
	<table class="table table-condensed table-hover table-striped" id="grid-user">
		<thead>
			<tr>
				<th data-column-id="id_user">User ID</th>
				<th data-column-id="full_name" data-formatter="full_name">Full Name</th>
				<th data-column-id="gender" data-formatter="gender">Gender</th>
				<th data-column-id="birth_date">Birth Date</th>
				<th data-column-id="birth_place">Birth Place</th>
				<th data-column-id="user_type_name">User Type</th>
				<th data-column-id="create_date">Create Date</th>

				<th data-column-id="action" data-formatter="action">Tools</th>
			</tr>
		</thead>
	</table>
<?php echo $this->template->cardBodyClose();?>

<script type="text/javascript">


	$(document).ready(function(){
	    $('.bootgrid-header .actionBar .actions').append($('#add-user'));

	    $("#add-user").click(function(){
	    	$("#addUserModal").modal('show');
	    	$("#user_id").val(0);
			$("#prefixModalUser").text('Add');
			$("#form-user")[0].reset();
	    });
	});


	$("#grid-user").bootgrid({
	    ajax: true,
	    url: "<?php echo site_url('user/user/loadList')?>",
	    formatters:{
	    	"action":function(column,row){
	    		txt = '';
	    		// Button Edit
	    		txt += '<button ';
	    		txt += '	class="btn btn-default btn-xs edit"';

	    		<?php foreach ($this->user_model->_column as $data): ?>

	    			txt += '<?php echo $data['columnName'] ?>="'+row.<?php echo $data['columnName']?>+'"';

	    		<?php endforeach ?>

	    		txt += '>';
	    		txt += '		<span class="fa fa-pencil"></span>';
	    		txt += '</button> ';

	    		// button delete

	    		txt += '<button ';
	    		txt += '	class="btn btn-danger btn-xs delete"';

	    		<?php foreach ($this->user_model->_column as $data): ?>

	    			txt += '<?php echo $data['columnName'] ?>="'+row.<?php echo $data['columnName']?>+'"';

	    		<?php endforeach ?>

	    		txt += '>';
	    		txt += '		<span class="fa fa-trash"></span>';
	    		txt += '</button>';


	    		return txt;
	    	},
	    	"gender":function(columnm,row){
	    		if(row.gender == 'f'){
	    			return 'Female';
	    		}else{
	    			return 'Male';
	    		}
	    	},
	    	"full_name":function(column,row){
	    		return row.first_name + ' ' + row.last_name;
	    	}
	    }
	}).on("loaded.rs.jquery.bootgrid", function (e) {
		$(".edit").click(function(){	
			$("#addUserModal").modal('show');

			<?php foreach ($this->user_model->_column as $data): ?>
				$("#<?php echo $data['columnName']?>").val(
					$(this).attr('<?php echo $data['columnName']?>')
				);
			<?php endforeach ?>
			$("#prefixModal").text('Edit');
		});

		$(".delete").click(function(){
			$("#deleteUserModal").modal('show');
			$("#deleteUserId").val($(this).attr('id_user'));
			$("#deleteUserName").text($(this).attr('first_name'));
		});
	});

	function deleteRow(){
		delete_id = $("#deleteUserId").val();
		$.ajax({
			url:"<?php echo site_url('user/user/delete')?>",
			data : { 
				id : delete_id,
			},
			type:"POST",
			success:function(res){
				res = JSON.parse(res);
				if(res.status){
					custom_notification('success','Success deleting data');
				}else{
					custom_notification('danger',res.info);
				}
				$(".modal").modal('hide');
				$("#grid-user").bootgrid('reload');
			}
		});
	}
</script>

<!-- Modal Add -->
<div id="addUserModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> <span id="prefixModal">Add</span> User Type</h4>
            </div>
            <div class="modal-body">
                <?php
                	echo $this->load->view('addUser');
                ?>
            </div>
            <div class="modal-footer">
            	<button class="btn btn-primary" onclick="saveUser()">
            		<span class="fa fa-check" id="symbSaveAddUserType"></span> Save 
            	</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Delete -->
<div id="deleteUserModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete User
            </div>
            <div class="modal-body">
            	Are you sure want to delete <b id="deleteUserName"></b> ? 
                <input type="hidden" name="deleteUserId" id="deleteUserId">
            </div>
            <div class="modal-footer">
            	<button class="btn btn-primary" onclick="deleteRow()">
            		Delete
            	</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

