<ul class="nav nav-tabs">
    <li  id="tab-userDetail" class="active">
    	<a data-toggle="tab" href="#userDetail">
    		<span class="fa fa-user"></span> User Detail
    	</a>
    </li>
    <li id="tab-userAccount">
    	<a data-toggle="tab" href="#userAccount">
    		<span class="fa fa-lock"></span> User Account
    	</a>
    </li>
    
    <!-- <li id="tab-userFiles" >
    	<a data-toggle="tab" href="#userFile">
    		<span class="fa fa-file"></span> User Files
    	</a>
    </li> -->
</ul>
<div class="tab-content">
    <div id="userDetail" class="tab-pane fade in active">
    	<br>
    	<br>
    	
    	<?php
    		echo $this->load->view('userSetting/UserDetail',get_defined_vars());
    	?>
    </div>
    <div id="userAccount" class="tab-pane fade">
        <br>
        <br>
        <?php
        	echo $this->load->view('userSetting/UserAccount');
        ?>
    </div>
    <div id="userFile" class="tab-pane fade">
		<br><br>
		<div class="col-md-9">
			<?php
				echo $this->load->view('userSetting/UserFile');
			?>
		</div>
    </div>
</div>

<!-- Row Account -->
	
<!-- Row Account -->

<script type="text/javascript">
	var rowAccount = $("#rowAccount");
	var rowUserDetail = $("#rowUserDetail");

	$(document).ready(function(){
		initDetail();
		// Account Save
		$("#rowAccount").submit(function(e){
			e.preventDefault();
			buttonSpinner('btnSaveAccount');
			$.ajax({
				url:"<?php echo site_url('rbac/account/save')?>",
				data : $("#rowAccount").serialize(),
				type:"POST",
				success:function(res){
					res = JSON.parse(res);
					if(res.status){
						custom_notification('info','<span class="fa fa-info"></span> Success updating account');
						initAccount();

					}else{
						custom_notification('danger',res.info);
					}
					removeButtonSpinner('btnSaveAccount');
				}
			});
		});
		$("#tab-userAccount").click(function(){
			initAccount();
		});
		$("#tab-userDetail").click(function(){
			initDetail();
		});
	});
	function initAccount(){
		$("#rowAccount").hide();
		$("#rowAccount").parent().append(spinner());
		$.ajax({
			url:"<?php echo site_url('rbac/account/load')?>",
			data : {
				username : "<?php echo $sessData['username']?>",
				mode : 'USERNAME',
			},
			type:"POST",
			success:function(res){
				res = JSON.parse(res);
				if(res.status){
					data = res.data;
					$("#username").val(data.username);
					$("#id_account").val(data.id_account);
					$("#account_status").val(data.account_status);
					$("#email").val(data.email);
					rowAccount.show();	
					removeSpinner();
				}
			}
		});
	}
</script>