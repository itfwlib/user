<table class="table table-hover table-stripped" id="grid-detailGroupMember">
	<thead>
		<tr>
			<th data-column-id="full-name" data-formatter="full-name">Full Name</th>
			<th data-column-id="create_date">Create Date</th>
		</tr>
	</thead>
</table>

<script type="text/javascript">
	function initDetailGroupMember(data){
		userGroupId = data.user_group_id;
		$("#grid-detailGroupMember").bootgrid({
			ajax : true,
			url:"<?php echo site_url('user/UserGroupMember/loadList') ?>",
			requestHandler: function (request) {
				request.user_group_id= userGroupId;
				return request;
			},
			formatters:{
				"full-name":function(column,row){
					return row.first_name + ' ' + row.last_name;
				}
			}
		});
		$("#grid-detailGroupMember").bootgrid('reload');
	}
</script>

