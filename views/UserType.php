<div id="add-user-type" class="btn-group">
    <button class="btn btn-default">
    	Add User Type
    </button>
</div>
<?php echo $this->template->cardOpen('');?>

<?php echo $this->template->cardBodyOpen();?>
	<table class="table table-condensed table-hover table-striped" id="grid-user-type">
		<thead>
			<tr>
				<th data-column-id="user_type_id">User Type ID</th>
				<th data-column-id="user_type_name">User Type Name</th>
				<th data-column-id="action" data-formatter="action">Tools</th>
			</tr>
		</thead>
	</table>
<?php echo $this->template->cardBodyClose();?>

<script type="text/javascript">


	$(document).ready(function(){
	    $("#add-user-type").click(function(){
	    	$("#addUserTypeModal").modal('show');
	    	$("#user_type_id").val(0);
			$("#prefixModal").text('Add');
			$("#form-user-type")[0].reset();
	    });
	});

	var table = $("#grid-user-type").DataTable({
		serverSide:true,
		processing:true,
		ajax : {
			url:"<?php echo site_url('user/UserType/loadList')?>",
			type:"POST"
		},
		columns : [
			{data:"user_type_id"},
			{data:"user_type_name"},
			{
				data:"tools",
				render:function(data,type,row,meta){
					txt = '';
					txt += '<button ';
					txt += '	class="btn btn-primary btn-xs edit"';
					txt += '	user_type_id="'+row.user_type_id+'"';
					txt += '	user_type_name="'+row.user_type_name+'"';
					txt += '>';
					txt += '	<span class="fa fa-pencil"></span>';
					txt += '</button>';

					txt += '<button ';
					txt += '	class="btn btn-danger btn-xs delete"';
					txt += '	user_type_id="'+row.user_type_id+'"';
					txt += '	user_type_name="'+row.user_type_name+'"';
					txt += '>';
					txt += '	<span class="fa fa-trash"></span>';
					txt += '</button>';


					return txt;
				}
			},
		]

	});
	$("#grid-user-type_filter").append($("#add-user-type"));
	$("#grid-user-type tbody").on('click','.edit',function(){
		$("#addUserTypeModal").modal('show');
		$("#user_type_id").val($(this).attr('user_type_id'));
		$("#user_type_name").val($(this).attr('user_type_name'));
		$("#prefixModal").text('Edit');
	});

	$("#grid-user-type tbody").on('click','.delete',function(){
		$("#deleteUserTypeModal").modal('show');
		$("#deleteIdUserType").val($(this).attr('user_type_id'));
		$("#deleteUserTypeName").text($(this).attr('user_type_name'));
	});

	function deleteRow(){
		delete_id = $("#deleteIdUserType").val();
		console.log(delete_id);
		$.ajax({
			url:"<?php echo site_url('user/userType/delete')?>",
			data : { 
				id : delete_id,
			},
			type:"POST",
			success:function(res){
				res = JSON.parse(res);
				if(res.status){
					custom_notification('success','Success deleting data');
				}else{
					custom_notification('danger',res.info);
				}
				$(".modal").modal('hide');
				$("#grid-user-type").DataTable().ajax.reload();
			}
		});
	}
</script>

<!-- Modal Add -->
<div id="addUserTypeModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> <span id="prefixModal">Add</span> User Type</h4>
            </div>
            <div class="modal-body">
                <?php
                	echo $this->load->view('addUserType');
                ?>
            </div>
            <div class="modal-footer">
            	<button class="btn btn-primary" onclick="saveUserType()">
            		<span class="fa fa-check" id="symbSaveAddUserType"></span> Save 
            	</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Delete -->
<div id="deleteUserTypeModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete User Type
            </div>
            <div class="modal-body">
            	Are you sure want to delete <b id="deleteUserTypeName"></b> ? 
                <input type="hidden" name="deleteIdUserType" id="deleteIdUserType">
            </div>
            <div class="modal-footer">
            	<button class="btn btn-primary" onclick="deleteRow()">
            		Delete
            	</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

