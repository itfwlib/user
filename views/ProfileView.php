
<?php echo $this->template->cardOpen('Profile');?>

<?php echo $this->template->cardBodyOpen();?>
	<div id="loaderProfileView" style="display: none;">
		<center><span class="fa fa-spinner fa-spin"></span> Loading...</center>
	</div>
	
	<div id="panelProfileView" style="display: none">
		<center>
			<img src="#" id="ProfileView_picutre" class="img-thumbnail" alt="Profile Picutre" width="304" height="236"> 
		</center>
		<br>
		<table class="table table-hover">
			<tr>
				<td class="text-right">Name</td>
				<td>:</td>
				<td id="ProfileView_name" ></td>
			</tr>
			<tr>
				<td class="text-right">Username</td>
				<td>:</td>
				<td id="ProfileView_username" ></td>
			</tr>
			<tr>
				<td class="text-right">Birth Date</td>
				<td>:</td>
				<td id="ProfileView_birth_date"></td>
			</tr>
			<tr>
				<td class="text-right">User Type</td>
				<td>:</td>
				<td id="ProfileView_user_type_name"></td>
			</tr>
		</table>
	</div>

	<div id="errorProfileView" style="display: none;">
		Sorry. please ask administrator to map your user to this account.
	</div>
<?php echo $this->template->cardBodyClose();?>

<script type="text/javascript">
	$(document).ready(function(){
		loadProfile();
	});	
	function loadProfile(data){
		$("#loaderProfileView").fadeIn();
		$.ajax({
			url:"<?php echo site_url('user/profile/loadProfile')?>",
			success:function(res){
				res = JSON.parse(res);
				if(res.status){
					data = res.data;
					var dateTime = new Date(data.birth_date);
					$("#ProfileView_name").text(data.first_name + ' ' + data.last_name);
					$("#ProfileView_username").text(data.username);
					$("#ProfileView_birth_date").text(moment(data.birth_date).format('DD MMMM YYYY'));
					$("#ProfileView_user_type_name").text(data.user_type_name);
					if( data.picture != null ){
						$("#ProfileView_picutre").attr('src',"<?php echo base_url('assets/user_upload/profile_picture/')?>"+data.picture);
					}else{
						$("#ProfileView_picutre").attr('src',"<?php echo base_url('assets/user_upload/profile_picture/default.png')?>");
					}
					$("#panelProfileView").fadeIn();
				}else{
					$("#errorProfileView").fadeIn();
				}
				$("#loaderProfileView").fadeOut();
			}
		});	

	}
</script>