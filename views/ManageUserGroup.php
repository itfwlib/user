<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			User Group Detail
		</div>
		<div class="panel-body">
			<table class="table table-hover table-stripped">
				<tr>
					<th>User Group ID</th>
					<th>:</th>
					<th id="userGroupIdDetail"></th>
				</tr>
				<tr>
					<th>User Group Name</th>
					<th>:</th>
					<th id="userGroupIdName"></th>
				</tr>
			</table>
		</div>
	</div>
</div>

<div class="col-md-6">
	<div class="panel panel-default">
		<div class="panel-heading">
			User
		</div>
		<div class="panel-body">
			<?php
				$this->load->view('userChooser',$data = array('callBackEvent' => 'addToGroup'));
			?>		
		</div>
	</div>
</div>

<div class="col-md-6">
	<div class="panel panel-default">
		<div class="panel-heading">
			Group Member
		</div>
		<div class="panel-body">
			<table class="table table-hover table-bordered">
				<?php
					$this->load->view('detailGroupMember');
				?>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	var id_group;
	var id_user;
	function initManageUserGroup(data){
		id_group = data.user_group_id;
		initDetailGroupMember(data);
		$("#userGroupIdDetail").text(data.user_group_id);
		$("#userGroupIdName").text(data.user_group_name);
	}
	function addToGroup(data){
		id_user = data.id_user;
		$.ajax({
			url:"<?php echo site_url('user/userGroupMember/save') ?>",
			data : {
				id_user : id_user,
				user_group_id : id_group,
			},
			type:"POST",
			success:function(res){
				res = JSON.parse(res);
				if(res.status){
					custom_notification('success','Success adding data');
				}else{
					custom_notification('danger','Failed Adding Data : '+res.info);
				}
				// initDetailGroupMember(data);
				$("#grid-detailGroupMember").bootgrid('reload');
			}
		});
	}
</script>