<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . '/modules/core/controllers/Controller.php';

class User extends Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('User_model','model');
	}
	public function save(){
		$params = $this->input->post();
		$column = $this->model->_column;

		foreach ($column as $column) {
			if($column['inputClass'] == 'rupiah'){
				$params[$column['columnName']] = format_angka($params[$column['columnName']]);
			}
		}
		$params['create_date'] = date('Y-m-d');
		$result = $this->model->save($params);
		echo json_encode($result);
	}
	public function delete(){
		$params = $this->input->post();
		$id = @$params['id'];
		$result = $this->model->delete($id);
		echo json_encode($result);
	}
	public function loadList(){
		$post = $this->input->post();

		$mode = isset($post['mode']) ? $post['mode'] : 'LOAD_ALL' ;
		$data = $this->model->load(null,$mode,$post);
		if(isset($post['current'])){
			$result = array(
				'current' => $post['current'],
				'rowCount' => $post['rowCount'],
				'rows' => $data['data']['rows']->result(),
				'total' => $data['data']['totalRows']);
		}else{
			$this->load->model('core/general_model');
			$result = $this->general_model->getResult();
			$result['data'] = $data['data']['rows']->result();
		}
		echo json_encode($result);
	}
	public function setting(){
		$sessData = $this->session->userdata();
		if($sessData['logged_in']){
			$this->grant();
		}
		
		$this->load->config('User_detail');
		$config = $this->config->item('User_detail');
		$userDetailColumn = $this->config->item('User_detail')['column'];

		$this->session->set_flashdata('page', 'Setting Profile');
		$this->dispatch(DEF_TEMPLATE_INSIDE,'SettingProfile',get_defined_vars());
	}
	public function load(){
		$post = $this->input->post();
		$mode = isset($post['mode']) ? $post['mode'] : 'LOADBY_ID';
		$this->load->model('core/general_model');
		$result = $this->general_model->getResult();
		if(count($post) < 1){
			$result['code'] = 10066;
			$result['status'] = false;
			$result['info'] = 'Please provide atleast 1 Parameters to load';
		}else{
			$result['data'] = $this->model->load(@$post['id'],$mode,$post);
		}
		echo json_encode($result);
	}
	public function test(){
		$this->grant();
		$this->dispatch(DEF_TEMPLATE_INSIDE,'userSetting/userDetail',get_defined_vars());
	}
	public function loadDatatable(){
		$post = $this->input->post();
		$datatable = $this->model->loadDatatable($post);
		$dataTable = array(
			'draw' => $post['draw'],
			'recordsTotal' => $datatable->recordsTotal,
			'recordsFiltered' => intval($datatable->recordsFiltered),
			'data' => $datatable->data 
		);
		echo json_encode($datatable);
		return true;
	}

}	