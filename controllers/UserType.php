<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . '/modules/core/controllers/Controller.php';

class UserType extends Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('User_type_model','model');
	}

	public function save(){
		$params = $this->input->post();
		$column = $this->model->_column;

		foreach ($column as $column) {
			if($column['inputClass'] == 'rupiah'){
				$params[$column['columnName']] = format_angka($params[$column['columnName']]);
			}
		}
		$result = $this->model->save($params);
		echo json_encode($result);
	}
	public function delete(){
		$params = $this->input->post();
		$id = @$params['id'];
		$result = $this->model->delete($id);
		echo json_encode($result);
	}
	public function loadList(){
		$post = $this->input->post();

		$data = $this->model->loadList($post);

		if(isset($post['draw'])){
			// datatable
			$result = array(
				'draw' => $post['draw'],
				'recordsTotal' => $data->total_records,
				'recordsFiltered' => $data->total_rows,
				'data' => $data->data
			);
		}else{
			$this->load->model('core/general_model');
			$result= $this->general_model->result();
			$result->data = $data;
		}
		echo json_encode($result);
		return true;
	}

}	