<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . '/modules/core/controllers/Controller.php';

class UserFile extends Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('UserFile_model','model');
	}

	public function save(){
		$params = $this->input->post();
		$column = $this->model->_column;

		foreach ($column as $column) {
			if($column['inputClass'] == 'rupiah'){
				$params[$column['columnName']] = format_angka($params[$column['columnName']]);
			}
		}
		$result = $this->model->save($params);
		echo json_encode($result);
	}
	public function delete(){
		$params = $this->input->post();
		$id = @$params['id'];
		$result = $this->model->delete($id);
		echo json_encode($result);
	}
	public function loadList(){
		$post = $this->input->post();
		$data = $this->model->load(null,'LOAD_ALL',$post);

		if(isset($post['current'])){
			$result = array(
				'current' => $post['current'],
				'rowCount' => $post['rowCount'],
				'rows' => $data['data']['rows']->result(),
				'total' => $data['data']['totalRows']);
		}else{
			$result = $data['data']['rows']->result();
		}
		echo json_encode($result);
	}
	public function getSetting(){
		$this->load->config('user_file');
		$config = $this->config->item('user_file');
		$this->load->model('core/general_model');
		$result = $this->general_model->getResult();
		// $result['status'] = false;
		$result['data'] = $config;
		echo json_encode($result);
	}

}	