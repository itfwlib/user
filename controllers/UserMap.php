<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . '/modules/core/controllers/Controller.php';

class UserMap extends Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('UserMap_model','model');
	}
	
	public function save(){
		$params = $this->input->post();
		$column = $this->model->_column;



		foreach ($column as $column) {
			if($column['inputClass'] == 'rupiah'){
				$params[$column['columnName']] = format_angka($params[$column['columnName']]);
			}
		}
		$params['create_date'] = date('Y-m-d');

		$this->form_validation->set_data($params);

		$this->form_validation->set_rules('id_account', 'ID Account', 'required|is_unique[user_map.id_account]', array(
			'is_unique' => 'This Account ('.$params['id_account'].') Already mapped to other account.')  );
		$this->form_validation->set_rules('id_user', 'ID User', 'is_unique[user_map.id_user]', array(
			'is_unique' => 'This User ('.$params['id_user'].') Already mapped to other user.'));

		if ($this->form_validation->run() == TRUE) {
			$result = $this->model->save($params);
		} else {
			$result['status'] = false;
			$result['info'] = validation_errors();
		}
		echo json_encode($result);
	}
	public function delete(){
		$params = $this->input->post();
		$id = @$params['id'];
		$result = $this->model->delete($id);
		echo json_encode($result);
	}
	public function loadList(){
		$post = $this->input->post();
		$data = $this->model->load(null,'LOAD_ALL',$post);

		if(isset($post['current'])){
			$result = array(
				'current' => $post['current'],
				'rowCount' => $post['rowCount'],
				'rows' => $data['data']['rows']->result(),
				'total' => $data['data']['totalRows']);
		}else{
			$result = $data['data']['rows']->result();
		}
		echo json_encode($result);
	}
	public function loadDatatable(){
		$post = $this->input->post();
		$datatable = $this->model->loadDatatable($post);
		$dataTable = array(
			'draw' => $post['draw'],
			'recordsTotal' => $datatable->recordsTotal,
			'recordsFiltered' => intval($datatable->recordsFiltered),
			'data' => $datatable->data 
		);
		echo json_encode($datatable);
		return true;
	}
	public function addUserGroup(){
		$this->grant();
		$this->dispatch(DEF_TEMPLATE_INSIDE,'manageUserGroup');
	}

}	