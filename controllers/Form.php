<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . '/modules/core/controllers/Controller.php';

class Form extends Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('user_model','user_model');
		$this->load->model('user_type_model');
		$this->using('jquery.bootgrid');
	}
	public function User(){
		$this->dispatch(DEF_TEMPLATE_INSIDE,'User',get_defined_vars());
	}
	public function UserType(){
		$this->using('datatable');
		$this->dispatch(DEF_TEMPLATE_INSIDE,'UserType',get_defined_vars());
	}
	public function UserGroup(){
		$this->dispatch(DEF_TEMPLATE_INSIDE,'UserGroup',get_defined_vars());
	}
	public function userMap(){
		// TODO :: Ubah datatable
		$this->using('datatable');
		$this->dispatch(DEF_TEMPLATE_INSIDE,'UserMap',get_defined_vars());
	}
	
}