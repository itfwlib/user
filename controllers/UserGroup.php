<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . '/modules/core/controllers/Controller.php';

class UserGroup extends Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('UserGroup_model','model');
	}
	
	public function save(){
		$params = $this->input->post();
		$column = $this->model->_column;

		foreach ($column as $column) {
			if($column['inputClass'] == 'rupiah'){
				$params[$column['columnName']] = format_angka($params[$column['columnName']]);
			}
		}

		$params['create_date'] = date('Y-m-d');
		$result = $this->model->save($params);
		echo json_encode($result);
	}
	public function delete(){
		$params = $this->input->post();
		$id = @$params['id'];
		$result = $this->model->delete($id);
		echo json_encode($result);
	}
	public function loadList(){
		$post = $this->input->post();
		$data = $this->model->load(null,'LOAD_ALL',$post);

		if(isset($post['current'])){
			$result = array(
				'current' => $post['current'],
				'rowCount' => $post['rowCount'],
				'rows' => $data['data']['rows']->result(),
				'total' => $data['data']['totalRows']);
		}else{
			$result = $data['data']['rows']->result();
		}
		echo json_encode($result);
	}
	public function addUserGroup(){
		$this->grant();
		$this->dispatch(DEF_TEMPLATE_INSIDE,'manageUserGroup');
	}

}	