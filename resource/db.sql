/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 10.1.37-MariaDB : Database - itfw
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(40) NOT NULL,
  `last_name` varchar(40) NOT NULL,
  `gender` char(1) NOT NULL,
  `birth_date` date NOT NULL,
  `birth_place` varchar(45) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `create_date` date NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `user` */


/*Table structure for table `user_detail` */

DROP TABLE IF EXISTS `user_detail`;

CREATE TABLE `user_detail` (
  `id_user_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `picture` text,
  `xs1` text,
  `xs2` text,
  `xs3` text,
  `xs4` text,
  `xs5` text,
  `xn1` int(11) DEFAULT NULL,
  `xn2` int(11) DEFAULT NULL,
  `xn3` int(11) DEFAULT NULL,
  `xd1` date DEFAULT NULL,
  `xd2` date DEFAULT NULL,
  `xd3` date DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id_user_detail`),
  KEY `FK_IDUSER_USERDETAIL` (`id_user`),
  CONSTRAINT `FK_IDUSER_USERDETAIL` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `user_file` (
  `id_user_file` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `file1` text,
  `file2` text,
  `file3` text,
  `file4` text,
  `file5` text,
  `picture1` text,
  `picture2` text,
  `picture3` text,
  `picture4` text,
  `picture5` text,
  PRIMARY KEY (`id_user_file`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_file` */

/*Table structure for table `user_group` */

DROP TABLE IF EXISTS `user_group`;

CREATE TABLE `user_group` (
  `user_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_name` text NOT NULL,
  `create_date` date NOT NULL,
  PRIMARY KEY (`user_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_group` */

/*Table structure for table `user_group_member` */

DROP TABLE IF EXISTS `user_group_member`;

CREATE TABLE `user_group_member` (
  `id_user_group_member` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `create_date` date NOT NULL,
  PRIMARY KEY (`id_user_group_member`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_group_member` */

/*Table structure for table `user_map` */

DROP TABLE IF EXISTS `user_map`;

CREATE TABLE `user_map` (
  `id_user_map` int(11) NOT NULL AUTO_INCREMENT,
  `id_account` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `create_date` date NOT NULL,
  PRIMARY KEY (`id_user_map`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


/*Table structure for table `user_type` */

DROP TABLE IF EXISTS `user_type`;

CREATE TABLE `user_type` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(35) NOT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `user_type` */

insert  into `user_type`(`user_type_id`,`user_type_name`) values 
(1,'Superadmin');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

insert  into `rbac_menu`(`id_menu`,`label`,`parent`,`references`,`type`,`icon`) values 
(6,'User',NULL,'#','0','fa fa-user'),
(7,'List User',6,'user/form/user','0','fa fa-users'),
(8,'User Type',6,'user/form/userType','0','fa fa-user-circle'),
(9,'User Map',6,'user/form/userMap','0','fa fa-address-book'),
(10,'User Group',6,'user/form/userGroup','0','fa fa-users');

/*Data for the table `rbac_privileges` */

insert  into `rbac_privileges`(`id_privileges`,`id_role`,`id_menu`,`update_dtm`) values 
(6,1,6,'2019-05-19 02:52:04'),
(7,1,7,'2019-05-19 02:52:58'),
(8,1,8,'2019-05-19 02:53:02'),
(9,1,9,'2019-05-19 02:53:07'),
(10,1,10,'2019-05-19 02:53:12');
