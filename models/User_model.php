<?php
	/**
	 * 
	 */
	class User_model extends CI_MODEL{
		protected $_table = 'user';
		protected $_key;
		public $_column;
		/* set this line to false if ID of table using custom prefix. */
		protected $_autoincrement = true;
		/* set this line to false if ID of table using custom prefix. */

		public function __construct(){
			parent::__construct();
			$CI =& get_instance();
			$CI->load->model('core/general_model');
			$this->_key = $CI->general_model->getKeyColoumn($this->_table);

			$this->_column = array(
				$this->map('id_user'),
				$this->map('first_name'),
				$this->map('last_name'),
				$this->map('gender','Gender',
					array(
							'type' => 'select' , // TYPE OF INPUT
							'id' => 'gender' , // ID OF OTHER MODEL COLUMN
							'name' => 'label' , // DATA WILL BE DISPLAYED
							'data' => array(
								array(
									'gender' => 'm',
									'label' => 'Male'),
								array(
									'gender' => 'f',
									'label' => 'Female')
							),
					)),
				$this->map('birth_date','Birth Date','date'),
				$this->map('birth_place'),
				$this->map('user_type_id','User Type ID',
					array(
							'type' => 'select' , // TYPE OF INPUT
							'id' => 'user_type_id' , // ID OF OTHER MODEL COLUMN
							'name' => 'user_type_name' , // DATA WILL BE DISPLAYED
							'model' => 'user/user_type_model', // namespace of model // set to null if data is static
					)
				),
			);
		}
		/* Mapp */
		public function map($columnName = '',$alias = null,$inputType = 'text' , $inputClass ='',$dataFormatting = '',$validation = null){
			if(is_array($inputType)){
				$type = $inputType['type'];
				$id = $inputType['id'];
				$name = $inputType['name'];
				// Load data into selected model
					if(isset($inputType['model'])){
						$aliasModel = str_replace('/','',strstr($inputType['model'],'/'));
						$this->load->model($inputType['model'],$aliasModel);
						$data = $this->$aliasModel->load()['data']['rows']->result();
					}else{
						$data = $this->convertArrayToObject($inputType['data']);
					}
				$inputType['data'] = $data;
			}
			return array(
				'columnName' => $columnName,
				'alias' => $alias,
				'inputType' => $inputType,
				'inputClass' => $inputClass,
				'dataFormatting' => '',
				'validation' => $validation
			);
		}
		/* Mapp */

		public function save($params){
			unset($params['id_user_detail']);
			unset($params['picture']);
			unset($params['xs1']);
			unset($params['xs2']);
			unset($params['xs3']);
			unset($params['xs4']);
			unset($params['xs5']);
			unset($params['xn1']);
			unset($params['xn2']);
			unset($params['xn3']);
			unset($params['xd1']);
			unset($params['xd2']);
			unset($params['xd3']);
			unset($params['last_update']);
			unset($params['id_user_map']);
			unset($params['id_account']);
			
			if( isset($params[$this->_key]) && !empty($params[$this->_key]) ){

				$id = $params[$this->_key];

				unset($params[$this->_key]);

				$this->db->set($params);
				$this->db->where($this->_key,$id);

				if($this->db->update($this->_table)){
					$result['status'] = true;
					$result['info'] = 'Berhasil Mengupdate data.';
					$result['data'] = $id;
				}else{
					$result['status'] = false;
					$result['info'] = 'Gagal Mengupdate Data';
				}
			}else{
				if($this->_autoincrement){
					unset($params[$this->_key]);
				}else{
					$CI =& get_instance();
					$CI->load->model('general_model');
					$params[$this->_key] = $CI->general_model->get_new_id_item($this->_table);
				}
				
				//insert data
				if($this->db->insert($this->_table,$params)){
					$result['status'] = true;
					$result['info'] = 'Berhasil Mengupdate data.';
					$result['data'] = $this->db->insert_id();
				}else{
					$result['status'] = false;
					$result['info'] = 'Berhasil Mengupdate data.';
				}
			}
			return $result;
		}

		public function load($id='',$mode = 'LOAD_ALL',$params = array()){

			$countAll = $this->db->from($this->_table)->count_all_results();

			if(isset($params['user_type_id'])){
				$this->db->where($this->_table.'.user_type_id',$params['user_type_id']);
			}

			if($mode != 'LOAD_ALL'){
				switch ($mode) {
					case 'LOADBY_ID':
						$this->db->where($this->_key,$id);
						break;
					case 'LOADBY_USERTYPEID':
						break;
					case 'NOTEXISTS_CLASSMEMBER':
						$this->db->where('NOT EXISTS (SELECT * FROM class_member WHERE class_member.id_user = '.$this->_table.'.id_user AND class_id = '.$params['class_id'].' )',null,false);
						break;
					case 'LOADBY_IDACCOUNT':
						$this->db->where('id_account', $params['id_account']);
						break;
				}
			}



			// If Bootgrid 
				if(isset($params['rowCount'])){
					$offset = ($params['current'] - 1) * $params['rowCount'];
					$this->db->limit($params['rowCount'],$offset);

					if(!empty($params['searchPhrase'])){
						foreach ($this->_column as $data) {
							$this->db->or_like($data['columnName'],$params['searchPhrase']);
						}
					}

				}
			// end if bootgrid

			$result = array();
			$this->db->join('user_type','user_type.user_type_id = '.$this->_table.'.user_type_id');
			if($data = $this->db->get($this->_table)){
				$result['status'] = true;
				$result['info'] = 'Success Loading data';
				$result['data'] = array(
					'rows' => $data,
					'totalRows' => $countAll
				);
			}
			return $result;
		}

		public function delete($id){
			$result = array();
			try{
				$this->db->where($this->_key,$id);
				if($this->db->delete($this->_table)){
					$result['status'] = true;
				}else{
					// $result['info'] = $this->db->last_query();
					$result['status'] = false;
				}
			}catch(\Exception $ex){
				// $result['info'] = $this->db->last_query();
				$result['status'] = false;
			}
			return $result;
		}

		public function getTable(){
			return $this->_table;
		}
		public function getKey(){
			return $this->_key;
		}
		public function convertArrayToObject($data){
			foreach ($data as $data2) {
				$obj[] =  (object) $data2;
			}
			return $obj;
		}

		public function loadDatatable($params){
			$this->db->start_cache();
			
			$this->db->from($this->_table);

			if($params['search']['value']){
				$this->db->or_where('lower(user.first_name) like "%'.$params['search']['value'].'%" ');
				$this->db->or_where('lower(user.last_name) like "%'.$params['search']['value'].'%" ');
				$this->db->or_where('lower(user.birth_place) like "%'.$params['search']['value'].'%" ');
			}

			$this->db->stop_cache();

			$recordsFiltered = $this->db->count_all_results();

			$this->db->limit(intval($params['length']));
			$this->db->offset(intval($params['start']));

			$data = $this->db->get();

			$result = new stdClass;
			$result->recordsFiltered = $recordsFiltered;
			$result->recordsTotal = $this->getRecordsTotal();
			$result->data = $data->result();
			return $result;

		}
		public function getRecordsTotal(){
			$this->db->select('COUNT(*) as countTotal');
			$q = $this->db->get($this->_table)->row()->countTotal;
			return $q;
		}
	}