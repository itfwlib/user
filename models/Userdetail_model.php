<?php
	/**
	 * 
	 */
	class UserDetail_model extends CI_MODEL{
		protected $_table = 'user_detail';
		protected $_key;
		public $_column;
		/* set this line to false if ID of table using custom prefix. */
		protected $_autoincrement = false;
		/* set this line to false if ID of table using custom prefix. */

		public function __construct(){
			/*
				MAPPING PARAMETERS : 
				$columnName , $alias , $inputType , $inputClass , $dataFormatting
				
				==================================================================

				IF INPUT IS FROM OTHER MODEL
				$inputType MUST BE AN ARRAY

				array(
					'type' => 'select' , // TYPE OF INPUT
					'id' => 'id_pegawai' , // ID OF OTHER MODEL COLUMN
					'name' => 'nama_pegawai' , // DATA WILL BE DISPLAYED
					'model' => 'dokter/model_dokter', // namespace of model // set to null if data is static

					'data' => array(
						array(
							'value' => 'Laki Laki',
							'label' => 'Laki Laki'),
						array(
							'value' => 'Perempuan',
							'label' => 'Perempuan')
					),  // only used for static data.
				);
			*/
			parent::__construct();
			$CI =& get_instance();
			$CI->load->model('general_model');
			$this->_key = $CI->general_model->getKeyColoumn($this->_table);

			$this->_column = array(
				$this->map('id_user_detail'),
				$this->map('id_user'),
				$this->map('picture'),
				$this->map('xs1'),
				$this->map('xs2'),
				$this->map('xs3'),
				$this->map('xs4'),
				$this->map('xs5'),
				$this->map('xn1'),
				$this->map('xn2'),
				$this->map('xn3'),
				$this->map('xd1'),
				$this->map('xd2'),
				$this->map('xd3'),
				$this->map('last_update'),
			);
		}
		/* Mapp */
		public function map($columnName = '',$alias = null,$inputType = 'text' , $inputClass ='',$dataFormatting = ''){
			if(is_array($inputType)){
				$type = $inputType['type'];
				$id = $inputType['id'];
				$name = $inputType['name'];
				// Load data into selected model
					if(isset($inputType['model'])){
						$aliasModel = str_replace('/','',strstr($inputType['model'],'/'));
						$this->load->model($inputType['model'],$aliasModel);
						$data = $this->$aliasModel->load()['data']->result();
					}else{
						$data = $this->convertArrayToObject($inputType['data']);
					}
				$inputType['data'] = $data;
			}
			return array(
				'columnName' => $columnName,
				'alias' => $alias,
				'inputType' => $inputType,
				'inputClass' => $inputClass,
				'dataFormatting' => '',
			);
		}
		/* Mapp */

		public function save($params){
			// unset
				unset($params['id_user_map']);
				unset($params['id_account']);
				unset($params['create_date']);
				unset($params['first_name']);
				unset($params['last_name']);
				unset($params['gender']);
				unset($params['birth_date']);
				unset($params['birth_place']);
				unset($params['user_type_id']);
			$params['last_update'] = date('Y-m-d H:i:s');

			if( isset($params[$this->_key]) && !empty($params[$this->_key]) ){

				$id = $params[$this->_key];

				unset($params[$this->_key]);

				$this->db->set($params);
				$this->db->where($this->_key,$id);

				if($this->db->update($this->_table)){
					$result['status'] = true;
					$result['info'] = 'Berhasil Mengupdate data.';
				}else{
					$result['status'] = false;
					$result['info'] = 'Gagal Mengupdate Data';
				}
			}else{
				// auto increment ?
				if($this->_autoincrement){
					unset($params[$this->_key]);
				}else{
					$CI =& get_instance();
					$CI->load->model('general_model');
					$params[$this->_key] = $CI->general_model->get_new_id_item($this->_table);
				}

				//insert data
				if($this->db->insert($this->_table,$params)){
					$result['status'] = true;
					$result['info'] = 'Berhasil Mengupdate data.';
				}else{
					$result['status'] = false;
					$result['info'] = 'Berhasil Mengupdate data.';
				}
			}
			return $result;
		}

		public function load($id='',$mode = 'LOAD_ALL',$params = array()){
			$countAll = $this->db->from($this->_table)->count_all_results();

			if($mode != 'LOAD_ALL'){
				switch ($mode) {
					case 'LOADBY_ID':
						$this->db->where($this->_key,$id);
						break;
					case 'LOADBY_IDUSER':
						$this->db->where('user_detail.id_user',$params['id_user']);
						break;
					case 'LOADBY_IDACCOUNT':
						$this->db->where('user_map.id_account',$params['id_account']);
						break;
				}
			}


			// If Bootgrid 
				if(isset($params['rowCount'])){
					$offset = ($params['current'] - 1) * $params['rowCount'];
					$this->db->limit($params['rowCount'],$offset);

					if(!empty($params['searchPhrase'])){
						foreach ($this->_column as $data) {
							$this->db->or_like($data['columnName'],$params['searchPhrase']);
						}
					}

				}
			// end if bootgrid

			$result = array();
			$this->db->join('user_map', 'user_map.id_user = user_detail.id_user');			
			$this->db->join('user', 'user.id_user = user_map.id_user');
			if($data = $this->db->get($this->_table)){
				// echo $this->db->last_query();die();	
				$result['status'] = true;
				$result['info'] = 'Success Loading data';
				$result['data'] = array(
					'rows' => $data,
					'totalRows' => $countAll
				);
			}
			return $result;
		}

		public function delete($id){
			$result = array();
			try{
				$this->db->where($this->_key,$id);
				if($this->db->delete($this->_table)){
					$result['status'] = true;
				}else{
					// $result['info'] = $this->db->last_query();
					$result['status'] = false;
				}
			}catch(\Exception $ex){
				// $result['info'] = $this->db->last_query();
				$result['status'] = false;
			}
			return $result;
		}

		public function getTable(){
			return $this->_table;
		}
		public function getKey(){
			return $this->_key;
		}
		public function convertArrayToObject($data){
			foreach ($data as $data2) {
				$obj[] =  (object) $data2;
			}
			return $obj;
		}
	}