<?php
	/**
	 * 
	 */
	class User_type_model extends CI_MODEL{
		protected $_table = 'user_type';
		protected $_key;
		public $_column;
		/* set this line to false if ID of table using custom prefix. */
		protected $_autoincrement = false;
		/* set this line to false if ID of table using custom prefix. */

		public function __construct(){
			/*
				MAPPING PARAMETERS : 
				$columnName , $alias , $inputType , $inputClass , $dataFormatting
				
				==================================================================

				IF INPUT IS FROM OTHER MODEL
				$inputType MUST BE AN ARRAY

				array(
					'type' => 'select' , // TYPE OF INPUT
					'id' => 'id_pegawai' , // ID OF OTHER MODEL COLUMN
					'name' => 'nama_pegawai' , // DATA WILL BE DISPLAYED
					'model' => 'dokter/model_dokter', // namespace of model // set to null if data is static

					'data' => array(
						array(
							'value' => 'Laki Laki',
							'label' => 'Laki Laki'),
						array(
							'value' => 'Perempuan',
							'label' => 'Perempuan')
					),  // only used for static data.
				);
			*/
			parent::__construct();
			$CI =& get_instance();
			$CI->load->model('general_model');
			$this->_key = $CI->general_model->getKeyColoumn($this->_table);

			$this->_column = array(
				$this->map('user_type_id','User Type ID'),
				$this->map('user_type_name','User Type Name')
			);
		}
		/* Mapp */
		public function map($columnName = '',$alias = null,$inputType = 'text' , $inputClass ='',$dataFormatting = ''){
			if(is_array($inputType)){
				$type = $inputType['type'];
				$id = $inputType['id'];
				$name = $inputType['name'];
				// Load data into selected model
					if(isset($inputType['model'])){
						$aliasModel = str_replace('/','',strstr($inputType['model'],'/'));
						$this->load->model($inputType['model'],$aliasModel);
						$data = $this->$aliasModel->load()['data']->result();
					}else{
						$data = $this->convertArrayToObject($inputType['data']);
					}
				$inputType['data'] = $data;
			}
			return array(
				'columnName' => $columnName,
				'alias' => $alias,
				'inputType' => $inputType,
				'inputClass' => $inputClass,
				'dataFormatting' => '',
			);
		}
		/* Mapp */

		public function save($params){
			if( isset($params[$this->_key]) && !empty($params[$this->_key]) ){

				$id = $params[$this->_key];

				unset($params[$this->_key]);

				$this->db->set($params);
				$this->db->where($this->_key,$id);

				if($this->db->update($this->_table)){
					$result['status'] = true;
					$result['info'] = 'Berhasil Mengupdate data.';
				}else{
					$result['status'] = false;
					$result['info'] = 'Gagal Mengupdate Data';
				}
			}else{
				// auto increment ?
				if($this->_autoincrement){
					unset($params[$this->_key]);
				}else{
					$CI =& get_instance();
					$CI->load->model('general_model');
					$params[$this->_key] = $CI->general_model->get_new_id_item($this->_table);
				}

				//insert data
				if($this->db->insert($this->_table,$params)){
					$result['status'] = true;
					$result['info'] = 'Berhasil Mengupdate data.';
				}else{
					$result['status'] = false;
					$result['info'] = 'Berhasil Mengupdate data.';
				}
			}
			return $result;
		}

		public function load($id='',$mode = 'LOAD_ALL',$params = array()){

			if($mode != 'LOAD_ALL'){
				switch ($mode) {
					case 'LOADBY_ID':
						$this->db->where($this->_key,$id);
						break;
				}
			}

			$countAll = $this->db->from($this->_table)->count_all_results();

			// If Bootgrid 
				if(isset($params['rowCount'])){
					$offset = ($params['current'] - 1) * $params['rowCount'];
					$this->db->limit($params['rowCount'],$offset);

					if(!empty($params['searchPhrase'])){
						foreach ($this->_column as $data) {
							$this->db->or_like($data['columnName'],$params['searchPhrase']);
						}
					}

				}
			// end if bootgrid

			$result = array();
			if($data = $this->db->get($this->_table)){
				// echo $this->db->last_query();die();	
				$result['status'] = true;
				$result['info'] = 'Success Loading data';
				$result['data'] = array(
					'rows' => $data,
					'totalRows' => $countAll
				);
			}
			return $result;
		}

		public function delete($id){
			$result = array();
			try{
				$this->db->where($this->_key,$id);
				if($this->db->delete($this->_table)){
					$result['status'] = true;
				}else{
					// $result['info'] = $this->db->last_query();
					$result['status'] = false;
				}
			}catch(\Exception $ex){
				// $result['info'] = $this->db->last_query();
				$result['status'] = false;
			}
			return $result;
		}

		public function getTable(){
			return $this->_table;
		}
		public function getKey(){
			return $this->_key;
		}
		public function convertArrayToObject($data){
			foreach ($data as $data2) {
				$obj[] =  (object) $data2;
			}
			return $obj;
		}
		public function loadList($params){
			$this->db->start_cache();

			$this->db->from($this->_table);

			if(isset($params['search']['value']) && $params['search']['value']){
				$this->db->where('lower(user_type_id) LIKE \'%'.strtolower($params['search']['value']).'%\'');
				$this->db->or_where('lower(user_type_name) LIKE \'%'.strtolower($params['search']['value']).'%\'');
			}

			$this->db->stop_cache();

			$total_rows = $this->db->count_all_results();

			if(isset($params['draw'])){
				// datatable
				$this->db->limit(intval($params['length']));
				$this->db->offset(intval($params['start']));

				$column = $params['order'][0]['column'];
				$dir = $params['order'][0]['dir'];
				$columnName = $params['columns'][$column]['data'];
				$this->db->order_by($columnName, $dir);

			}

			$query = $this->db->get();
			// echo $this->db->last_query();die();
			$result = new stdClass;
			$result->total_rows = $total_rows;
			$result->data = $query->result();
			$result->total_records = $this->getTotalRecords();

			return $result;
		}

		public function getTotalRecords(){
			$this->db->select('COUNT(*) as totalRecords');
			$q = $this->db->get($this->_table)->row();
			return $q->totalRecords;
		}
	}