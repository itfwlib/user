<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['user_file'] = array(
	'file' => array(
		0 => array(
			'name' => 'file1',
			'alias' => 'Raport Lembar 1',
			'show' => true,
		),
		1 => array(
			'name' => 'file2',
			'alias' => 'Raport Lembar 2',
			'show' => true,
		),
		2 => array(
			'name' => 'file3',
			'alias' => 'testfile3',
			'show' => true,
		),
		3 => array(
			'name' => 'file4',
			'alias' => 'testfile4',
			'show' => true,
		),
		4 => array(
			'name' => 'file5',
			'alias' => 'testfile5',
			'show' => true,
		),
	),
	'picture' => array(
		0 => array(
			'name' => 'picture1',
			'alias' => 'Foto KTP',
			'show' => true,
		),
		1 => array(
			'name' => 'picture2',
			'alias' => 'testPicture3',
			'show' => true,
		),
		2 => array(
			'name' => 'picture3',
			'alias' => 'testPicture4',
			'show' => true,
		),
		3 => array(
			'name' => 'picture4',
			'alias' => 'testPicture5',
			'show' => true,
		),
		4 => array(
			'name' => 'picture5',
			'alias' => 'example-date-3',
			'show' => true,
		),
		6 => array(
			'name' => 'picture6',
			'alias' => 'testPicture2',
			'show' => true,
		),
		7 => array(
			'name' => 'picture7',
			'alias' => 'testPicture3',
			'show' => true,
		),
		8 => array(
			'name' => 'picture8',
			'alias' => 'testPicture4',
			'show' => true,
		),
		9 => array(
			'name' => 'picture9',
			'alias' => 'testPicture5',
			'show' => true,
		),
		10 => array(
			'name' => 'picture10',
			'alias' => 'example-date-3',
			'show' => true,
		),
	),

);