<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['user_detail'] = array(
	'column' => array(
		1 => array(
			'name' => 'xs1',
			'alias' => 'Hobby',
			'show' => true,
			'type' => 'text',
		),
		2 => array(
			'name' => 'xs2',
			'alias' => 'Address',
			'show' => true,
			'type' => 'text',
		),
		3 => array(
			'name' => 'xs3',
			'alias' => 'Phone Number',
			'show' => true,
			'type' => 'text',
		),
		4 => array(
			'name' => 'xs4',
			'alias' => 'example-string-4',
			'show' => false,
			'type' => 'text',
		),
		5 => array(
			'name' => 'xs5',
			'alias' => 'example-string-5',
			'show' => false,
			'type' => 'text',
		),
		6 => array(
			'name' => 'xn1',
			'alias' => 'Favorite Number',
			'show' => true,
			'type' => 'number',
		),
		7 => array(
			'name' => 'xn2',
			'alias' => 'example-number-2',
			'show' => false,
			'type' => 'number',
		),
		8 => array(
			'name' => 'xn3',
			'alias' => 'example-number-3',
			'show' => false,
			'type' => 'number',
		),
		9 => array(
			'name' => 'xd1',
			'alias' => 'Join Date',
			'show' => true,
			'type' => 'date',
		),
		10 => array(
			'name' => 'xd2',
			'alias' => 'example-date-2',
			'show' => false,
			'type' => 'date',
		),
		11 => array(
			'name' => 'xd3',
			'alias' => 'example-date-3',
			'show' => false,
			'type' => 'date',
		),
	)
);